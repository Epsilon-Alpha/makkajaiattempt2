class Process
{
    public static void checkFirstDigit(char c)
    {
        try
        {
            if (!Character.isDigit(c))
                throw new Exception("Invalid format");
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    //Take a line and retrieve item quantity from it
    public static int fetchQuantity(String line)
    {
        int quantity = 0;
        for (int i = 0; line.charAt(i) != ' '; i++)
            quantity = quantity * 10 + (line.charAt(i) - '0');
        return quantity;
    }

    //Take a line and a reference index and retrieve item name from it
    public static String fetchItemName(String line, int idx)
    {
        int spaceIndex = line.indexOf(' ');
        return line.substring(spaceIndex+1, idx-1);
    }

    //Take a line and a reference index and retrieve cost from it
    public static String fetchCost(String line, int idx)
    {
        return line.substring(idx + 3);
    }

    //Convert string to lowercase for string matching and match whitelisted strings
    public static boolean stringFoundWithinWhitelist(String item)
    {
        boolean taxExempt = false;
        String itemLower = item.toLowerCase();
        for (String S : Constants.WHITELIST)
        {
            if (itemLower.contains(S))
            {
                taxExempt = true;
                break;
            }
        }
        return taxExempt;
    }

    public static double calculateTax(boolean taxExempt, double cost, String line)
    {
        double currentTax = 0;
        if (!taxExempt)
            currentTax += (Constants.TAXPERCENT / 100.0 * cost);
        if (line.contains("imported"))
            currentTax += (Constants.IMPORTPERCENT / 100.0 * cost);
        currentTax = Math.ceil(currentTax * 20) / 20.0;
        return currentTax;
    }

    public static void calculate()
    {
        double totalTax = 0, total = 0;
        for (String line : FileReader.lines)
        {
            checkFirstDigit(line.charAt(0));

            //Find last index of the string "at"
            int idx = line.lastIndexOf("at ");

            //Fetch Quantity
            int quantity = fetchQuantity(line);

            //Fetch item name
            String item = fetchItemName(line,idx);

            //Fetch cost
            String costString = fetchCost(line,idx);
            try
            {
                double cost = Double.parseDouble(costString);
                boolean taxExempt = stringFoundWithinWhitelist(item);
                double currentTax = calculateTax(taxExempt, cost, line);
                totalTax += currentTax;
                double costTaxSum = cost + currentTax;
                total += costTaxSum;
                System.out.println(quantity + " " + item + ": " + Constants.df.format(costTaxSum));
            }
            catch (NumberFormatException nfe)
            {
                System.out.println("Error handling number conversion");
                nfe.printStackTrace();
            }
        }
        System.out.println("Sales Tax: " + Constants.df.format(totalTax));
        System.out.println("Total: " + Constants.df.format(total));
        System.out.println();
    }

    public Process(String fileName)
    {
        FileReader fr = new FileReader(fileName);
        calculate();
    }
}