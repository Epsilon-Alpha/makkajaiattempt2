import java.text.DecimalFormat;
import java.util.List;

//Class having a private constructor, showing usage of a Singleton class
public final class Constants
{
    final static int TAXPERCENT = 10;
    final static int IMPORTPERCENT = 5;
    final static DecimalFormat df = new DecimalFormat("0.00");
    final static String[] WHITELIST_ARRAY = {"book","chocolate","pills"};
    public static final List<String> WHITELIST = List.of(WHITELIST_ARRAY);

    private Constants() {
    }
}

