import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

class FileReader
{
    static ArrayList<String> lines;

    public FileReader(String filename)
    {
        lines = new ArrayList<String>();
        readFile(filename);
    }

    public static void readFile(String filename)
    {
        try
        {
            File newFile = new File(filename);
            Scanner sc = null;
            try
            {
                sc = new Scanner(newFile);
            }
            catch (FileNotFoundException fileNotFoundException)
            {
                fileNotFoundException.printStackTrace();
            }
            while (sc.hasNextLine())
            {
                String line = sc.nextLine();
                lines.add(line);
            }
            sc.close();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}